package com.example.ciudadjoven;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;

/**
 * Clase para el fragmento detalle de una actividad
 */
public class DetailedFragment extends Fragment {

    private TextView title, description, location, price;
    private ImageView imageView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detailfragment, container, false);

        //Obtenemos los valores
        title = view.findViewById(R.id.detalle_titulo);
        description = view.findViewById(R.id.detalle_descripcion);
        location = view.findViewById(R.id.detalle_ubicacion);
        price = view.findViewById(R.id.detalle_precio);
        imageView = view.findViewById(R.id.detalle_imagen);

        //Recuperamos el objeto enviado desde el fragment
        Bundle activityObject = getArguments();
        Activity activity;
        if(activityObject != null){
            activity = (Activity) activityObject.getSerializable("objeto");
            title.setText(activity.getTitle());
            description.setText(activity.getDescription());
            location.setText(activity.getLocation());
            price.setText(activity.getPrice());
            imageView.setImageResource(activity.getImage());
        }
        return view;
    }


}
