package com.example.ciudadjoven;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

//Adaptador
public class AdapterActivity extends RecyclerView.Adapter<AdapterActivity.ViewHolder> implements View.OnClickListener{

    private ArrayList<Activity> model;
    private LayoutInflater inflater;
    private View.OnClickListener listener;

    public AdapterActivity(Context context, ArrayList<Activity> model) {
        this.inflater = LayoutInflater.from(context);
        this.model = model;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.lista_actividades, parent, false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterActivity.ViewHolder holder, int position) {
        String titulo = model.get(position).getTitle();
        String descripcion = model.get(position).getDescription();
        int imagen = model.get(position).getImage();
        holder.titulo.setText(titulo);
        holder.descripcion.setText(descripcion);
        holder.imagen.setImageResource(imagen);
    }

    @Override
    public int getItemCount() {
        return model.size();
    }

    @Override
    public void onClick(View view) {
        if(listener != null) listener.onClick(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView titulo, descripcion;
        private ImageView imagen;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            titulo = itemView.findViewById(R.id.titulo_actividad);
            descripcion = itemView.findViewById(R.id.descripcion_actividad);
            imagen = itemView.findViewById(R.id.imagen_actividad);
        }
    }
}
