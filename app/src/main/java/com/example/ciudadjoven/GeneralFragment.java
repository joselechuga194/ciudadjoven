package com.example.ciudadjoven;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

/**
 * Clase para el fragmento general
 */
public class GeneralFragment extends Fragment {

    private AdapterActivity adapterActivity;
    private RecyclerView recyclerViewActivities;
    private ArrayList<Activity> activityList;
    private Bundle args;
    private TextView header, description;

    //Variables para la comunicación entre fragments
    private android.app.Activity activity;
    private ILinkFragments iLinkFragments;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.generalfragment, container, false);
        recyclerViewActivities = view.findViewById(R.id.recycleView);
        activityList = new ArrayList<>();

        //Esto es para poner el header y descripcion segun la seccion que se seleccione
        args = getArguments();
        header = (TextView) view.findViewById(R.id.header_title);
        description = (TextView) view.findViewById(R.id.header_description);
        header.setText(args.getString("header"));
        description.setText(args.getString("desc"));

        //Cargamos la lista de actividades
        loadList(args.getString("header"));

        //Mostramos los datos
        showData();
        return view;
    }

    //Lista de actividades para cada categoría.
    public void loadList(String act){
        switch (act){
            case "Debates":
                activityList.add(new Activity("Un paso hacía la libertad", "Debate sobre la libertad de expresión en México", "Hidalgo 45, Centro Histórico, CDMX 06010\n05-09-2021", 1,  R.drawable.debate1, "Entrada gratuita"));
                activityList.add(new Activity("Acoso escolar", "Debate sobre el problema de acoso escolar en la actualidad", "Hidalgo 85, Centro Histórico, CDMX 06300\n19-09-2021", 1,  R.drawable.debate2, "Entrada gratuita"));
                activityList.add(new Activity("Responsabilidad afectiva", "¿Qué es la responsabilidad afectiva y por qué es importante?", "Zamora 7, Condesa, CDMX 06140\n02-10-2021", 1,  R.drawable.debate3, "Entrada gratuita"));
                activityList.add(new Activity("Vivas nos queremos", "Debate sobre la violencia de género en México", "Tlalpan 79, Country Club, CDMX 04020\n04-20-2021", 1,  R.drawable.debate4, "Entrada gratuita"));
                activityList.add(new Activity("Educación sexual", "La importancia de la educación sexual en las escuelas públicas", "República de Cuba 60, Centro Histórico, CDMX 06000\n25-10-2021", 1,  R.drawable.debate5, "Entrada gratuita"));
                break;
            case "Exposiciones":
                activityList.add(new Activity("De barro y de silencio", "La voz de los periodistas que arriesgan su vida", "Dr. Atl 62, Santa María la Ribera, CDMX, 06400\nHasta el 20-09-2021", 1,  R.drawable.expo1, "Entrada gratuita"));
                activityList.add(new Activity("Frida", "Exposición inmersiva y multisensorial para sumergirte en la vida y obra de Frida Kahlo", "Plaza de La República s/n, Tabacalera, CDMX, 06030", 1,  R.drawable.expo2, "Entrada gratuita"));
                activityList.add(new Activity("Da Vinci experience", "Recuerda a Leonardo da Vinci a 500 años de su muerte en esta exposición multimedia e inmersiva", "Lago Zurich 245, Ampliación Granada, CDMX 11529\nHasta el 31 de agosto de 2021", 1,  R.drawable.expo3, "$95 por persona"));
                activityList.add(new Activity("Luz instante", "Instalaciones de Julia Carrillo sobre la luz, la materia el tiempo y el espacio", "Artz Pedregal: Periférico Sur 3720, CDMX 01900\nHasta el 10 de septiembre de 2021", 1,  R.drawable.expo4, "$35 por persona"));
                activityList.add(new Activity("Mapplethorpe", "Prepárate para descubrir las fotografías eróticas monocromáticas del artista estadounidense", "Galerias Morán Morán: Horacio 1022, Polanco, CDMX\nHasta el 8 de agosto de 2021", 1,  R.drawable.expo5, "$35 por persona"));
                activityList.add(new Activity("Dalí 2.1", "El artista surrealista llega a México como nunca antes se había visto en el país", "Galería Daliana: Vasco de Quiroga 3800, Lomas de Santa Fe, CDMX\nHasta el 30 de junio de 2021", 1,  R.drawable.expo6, "$150 - $325"));
                break;
            case "Talleres":
                activityList.add(new Activity("El individuo y su identidad reflejada en el cuerpo ", "Taller de introducción al tatuaje ", "Av. Pino Suarez #30, Colonia Centro, CDMX, 06060\n12-08-2021", 1,  R.drawable.taller1, "Entrada gratuita con credencial de estudiante\n$50 por persona"));
                activityList.add(new Activity("Taller de baile", "Curso taller de danza contemporánea", "Av. Pino Suarez #30, Colonia Centro, CDMX, 06060\n12 y 14 de agosto", 1,  R.drawable.taller2, "$100 por persona"));
                activityList.add(new Activity("Un verano para ti", "El Museo de los Ferrocarrileros ofrece un amplio programa de actividades lúdicas en este verano", "Av. Ricardo Flores Magón 206, Buenavista, CDMX 06350\n Hasta el 29 de septiembre de 2021", 1,  R.drawable.taller3, "Entrada gratuita"));
                activityList.add(new Activity("LGBT+ allá del arcoíris", "La importancia de adoptar la diversidad y la tolerancia como parte de nuestra cultura", "Luis Moya 12, Centro Histórico, CDMX 06010", 1,  R.drawable.taller4, "$69 por persona"));
                break;
            case "Museos":
                activityList.add(new Activity("Museo Soumaya", "Exhibe más de seis mil piezas de arte de la Fundación Carlos Slim.", "Blvd. Miguel de Cervantes Saavedra 303, Granada, CDMX 11528", 1,  R.drawable.museo1, "Entrada gratuita"));
                activityList.add(new Activity("Museo del Palacio de Bellas Artes", "El máximo recinto cultural de país y escenario de los más destacados artsitas", "Av. Juaréz s/n, Colonia Centro, CDMX 06050", 1, R.drawable.museo2, "El precio varia de acuerdo a la exposición"));
                activityList.add(new Activity("Museo del la luz", "El espacio donde se exploran diferentes facetas del fenómeno de la luz.", "San Ildefonso 43, Colonia Centro, CDMX 06020", 1, R.drawable.museo3, "Desde $35"));
                activityList.add(new Activity("Museo del Tatuaje", "Reúne 25 años de experiencia y conocimiento sobre lo que significa el tatuaje en el panorama nacional e internacional.", "Insurgentes Sur 221, Colonia Roma, CDMX 06700", 1, R.drawable.museo4, "Entrada gratuita"));
                activityList.add(new Activity("Museo del Automóvil", "La colección consta de más de 120 automóviles y busca mostrar los avances de la industria automotriz.", "Av. División del Norte 3572, San Pablo, CDMX 04620", 1, R.drawable.museo5, "Desde $30"));
                activityList.add(new Activity("Universum", "El primer museo en México dedicado a promover la ciencia y la tecnología.", "Universidad Nacional Autónoma de México, Ciudad Universitaria, Cto. Centro Cultural de, C.U., Coyoacán, CDMX 04510", 1, R.drawable.museo6, "Entrada general: $90\nMiembros UNAM: $80"));
                activityList.add(new Activity("Museo Memoria y Tolerancia", "A través de la memoria histórica, las salas muestran un recorrido por los peores crímenes que ha cometido la humanidad contra sí misma por la intolerancia religiosa, étnica y de otros tipos.", "Juaréz 8, Colonia Centro, CDMX 06000", 1, R.drawable.museo7, "Desde $95"));
                break;
            case "Conciertos":
                activityList.add(new Activity("Misterios mexicanos", "Este proyecto promueve la obra de autores y artistas visuales relacionados con el jazz y la música contemporánea en este país.", "Cenart: Río Churubusco 79, Country Club, CDMX 4220\n26-08-2021 8:00pm", 1, R.drawable.concierto1, "$120"));
                activityList.add(new Activity("Festival Hipnosis 2021", "Cuarta edición del festival de música psicodélica, garage y stoner.", "Camino a San Ramón s/n Dos ríos, Estado de México, 52790\nDel 04 al 06 de noviembre de 2021", 1, R.drawable.concierto2, "$1150 - $2350"));
                activityList.add(new Activity("México en la piel", "Festival de música folklorica y regional", "Av. Pino Suarez #30, Colonia Centro, CDMX, 06060\nDel 02 al 18 de septiembre de 2021", 1, R.drawable.concierto5, "Entrada gratuita"));
                activityList.add(new Activity("Luz y música", "Festival de música electrónica acompañada de show de luces ", "Blvd. Miguel de Cervantes Saavedra 303, Granada, CDMX 11528\nDel 24 de septiembre al 02 de octubre de 2021", 1, R.drawable.concierto3, "$150 - $200"));
                activityList.add(new Activity("Keane en concierto", "Keane visita México por primera vez en su imperdible gira mundial", "Av, Viaducto Río de la Piedad s/n, Granjas México, CDMX 08400\n12-11-2021 9:30pm", 1, R.drawable.concierto4, "$1500 - $3200"));
                break;
            case "Cine":
                activityList.add(new Activity("Adam", "Adam es un joven con problemas de audición que se enfrenta a un dilema cuando su madre alcohólica es hospitalizada y se le diagnostica un daño cerebral...", "Cineteca Nacional: Av. México Coyoacán 389, Xoco, CDMX 03330\nHasta el 20 de agosto", 1, R.drawable.cine1, "$50"));
                activityList.add(new Activity("Shirley", "Después de aceptar una invitación a vivir en casa del profesor Stanley Hyman, Fred y Rose se ven atrapados en un inquietante experimento de la escritora Shirley Jackson...", "Cineteca Nacional: Av. México Coyoacán 389, Xoco, CDMX 03330\nHasta el 20 de agosto", 1, R.drawable.cine2, "$50"));
                activityList.add(new Activity("Old", "Escalofriante y misterioso thriller sobre una familia en sus vacaciones, quienes descubren que la aislada playa donde se relajan, de alguna manera, los está haciendo envejecer de manera acelerada...", "Disponible en complejos Cinemex y Cinepolis", 1, R.drawable.cine3, "Desde $75"));
                activityList.add(new Activity("Free Guy", "Un cajero de banco descubre que, en realidad, es un jugador secundario de un videojuego de mundo abierto...", "Disponible en complejos Cinemex y Cinepolis", 1, R.drawable.cine4, "Desde $75"));
                activityList.add(new Activity("The prestige", "Un encuentro durante una sesión fraudulenta provoca que dos magos del siglo XIX, se enfrenten en una intensa batalla por la supremacía.", "Autocinema Coyote: Lago Zurich 200, Granada, CDMX 11529\n18-08-2021 8:30pm", 1, R.drawable.cine5, "$290 por automovil"));
                activityList.add(new Activity("Amores Perros", "Una historia audaz, de gran intensidad emocional y ambición, que entrelaza y explora las vidas de tres personajes dispares a partir de un accidente automovilístico en la Ciudad de México...", "Autocinema Coyote: Av. de los Insurgentes Sur 1729, Guadalupe Inn, CDMX 01020\n08-09-2021 8:30pm", 1, R.drawable.cine6, "$290 por automovil"));
                break;
            case "Restaurantes":
                activityList.add(new Activity("Pizza Jazz Café", "Este club clandestino de jazzistas es muy íntimo y solo abre sus puertas por las noches. Sus pizzas son hechas en horno de piedra y de vez en cuando también tienen panadería artesanal.", "Eje 7 Sur 46, Portales Oriente\nAbierto de lunes a domingo de 19:30 a 23:00", 1, R.drawable.res1, "Desde $150 a $200 por persona"));
                activityList.add(new Activity("The Secret Donut Society", "Créenos cuando te decimos que es toda una experiencia comprar en este lugar.", "Tabasco 262, Roma Norte, CDMX 06700\nAbierto de lunes a domingo de 10:00 a 22:00", 1, R.drawable.res2, "Desde $45 por persona"));
                activityList.add(new Activity("El Diálogo de los Ángeles", "Escondido en una vecindad del Centro, es uno de los restaurantes y cafés secretos en la CDMX más lindos.", "República de Guatemala 82, Centro Histórico, CDMX 06020\nAbierto de lunes a viernes de 10:00 a 17:15 y sábado de 10:00 a 15:15", 1, R.drawable.res3, "Desde $100 a $200 por persona"));
                activityList.add(new Activity("Broka", "Un oasis en la ciudad que nos permite hacer una pausa para reconfortarnos con una propuesta gastronómica única.", "Zacatecas 126B, Roma Norte, CDMX 06700\nAbierto de lunes a domingo de 14:00 a 23:00", 1, R.drawable.res4, "Desde $100 a $300 por persona"));
                activityList.add(new Activity("La Caravana Gamelab", "Cafetería y juegos de mesa, ¿qué más podríamos pedir?", "Fresas 59, Tlacoquemécatl del Valle, CDMX 03200\nAbierto de lunes a domingo de 10:00 a 21:00", 1, R.drawable.res5, "Desde $50 por persona"));
                activityList.add(new Activity("Cafetería de la Gran ciudad", "Un delicioso café y una mirada panorámica a la Ciudad de México desde la Torre Latino", "Eje Central 2 Piso 9, Centro Histórico, CDMX 05000\nAbierto de lunes a sábado de 10:00 a 21:00", 1, R.drawable.res6, "Desde $50 por persona"));
                break;
            case "Más":
                activityList.add(new Activity("Xochimilco", "Una pequeña Venecia en la Ciudad de México", "Periferico Oriente 1, Ciénega Grande, CDMX", 1, R.drawable.mas1, "Entrada gratuita"));
                activityList.add(new Activity("Fastlove", "Compra ropa vintage y de segunda mano", "Alvaro Obregón 159, Roma Norte, CDMX 06700", 1, R.drawable.mas2, "Sin costo"));
                activityList.add(new Activity("Librería El Péndulo", "Un café y un libro de están esperando en está hermosa librería", "Alejandro Dumas 81, Polanco, CDMX 11560", 1, R.drawable.mas3, "Sin costo"));
                break;
        }
    }

    public void showData(){
        recyclerViewActivities.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterActivity = new AdapterActivity(getContext(), activityList);
        recyclerViewActivities.setAdapter(adapterActivity);
        adapterActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Enviamos el objeto seleccionado al fragment de detalles
                iLinkFragments.linkActivity(activityList.get(recyclerViewActivities.getChildAdapterPosition(view)));
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(context instanceof android.app.Activity){
            this.activity = (android.app.Activity) context;
            iLinkFragments = (ILinkFragments) this.activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
