package com.example.ciudadjoven;

import java.io.Serializable;

public class Activity implements Serializable {
    private final String title;
    private final String description;
    private final String location;
    private final String price;
    private final int image;

    /**
     *
     * @param title
     * @param description
     * @param location
     * @param price
     * @param section Identifica a que tipo de actividad pertenece la actividad
     * @param image
     */
    public Activity(String title, String description, String location, int section, int image, String price) {
        this.title = title;
        this.description = description;
        this.location = location;
        this.image = image;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLocation() {
        return location;
    }

    public int getImage() {
        return image;
    }

    public String getPrice(){return price;}
}
