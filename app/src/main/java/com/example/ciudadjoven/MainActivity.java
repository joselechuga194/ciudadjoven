package com.example.ciudadjoven;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ILinkFragments {

    private DrawerLayout drawerLayout;
    private DetailedFragment detailedFragment;
    private GeneralFragment generalFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();

        drawerLayout = findViewById(R.id.nav_view);
        Toolbar toolbar = findViewById(R.id.toolbar);

        //Manejamos las distintas vistas (fragmentos)
        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment());

        //Acciones para manejar el menu lateral
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,
                drawerLayout, toolbar, R.string.navigation_draw_open, R.string.navigation_draw_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

    }

    /**
     * Aquí se selecciona cual fragmento mostrar segun la actividad seleccionada
     * @param item
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        generalFragment = new GeneralFragment();
        Bundle args = new Bundle();

        //Sirve para manejar el boton de regreso y que no se cierre la aplicacion al pulsarlo agregando las transiciones al stack
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction().addToBackStack(null);

        switch (item.getItemId()){
            case R.id.debates:
                args.putString("header", "Debates");
                args.putString("desc", "Debates abiertos en la ciudad");
                generalFragment.setArguments(args);
                transaction.replace(R.id.fragment_container, generalFragment).commit();
                break;
            case R.id.exposiciones:
                args.putString("header", "Exposiciones");
                args.putString("desc", "Encuentra las exposiciones más interesantes");
                generalFragment.setArguments(args);
                transaction.replace(R.id.fragment_container, generalFragment).commit();
                break;
            case R.id.talleres:
                args.putString("header", "Talleres");
                args.putString("desc", "¿Ganas de aprender algo nuevo?, prueba estos talleres");
                generalFragment.setArguments(args);
                transaction.replace(R.id.fragment_container, generalFragment).commit();
                break;
            case R.id.museos:
                args.putString("header", "Museos");
                args.putString("desc", "La ciudad de México tiene muchos museos, ¡Visitalos!");
                generalFragment.setArguments(args);
                transaction.replace(R.id.fragment_container, generalFragment).commit();
                break;
            case R.id.conciertos:
                args.putString("header", "Conciertos");
                args.putString("desc", "Conciertos para disfrutar el momento");
                generalFragment.setArguments(args);
                transaction.replace(R.id.fragment_container, generalFragment).commit();
                break;
            case R.id.cine:
                args.putString("header", "Cine");
                args.putString("desc", "El septimo arte esta en la ciudad");
                generalFragment.setArguments(args);
                transaction.replace(R.id.fragment_container, generalFragment).commit();
                break;
            case R.id.restaurantes:
                args.putString("header", "Restaurantes");
                args.putString("desc", "Prueba la variada gastronomia de la ciudad");
                generalFragment.setArguments(args);
                transaction.replace(R.id.fragment_container, generalFragment).commit();
                break;
            case R.id.mas:
                args.putString("header", "Más");
                args.putString("desc", "Actividades extras");
                generalFragment.setArguments(args);
                transaction.replace(R.id.fragment_container, generalFragment).commit();
                break;
            default:
                cleanFragmentStack();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                break;
        }

        //Cerramos el drawer despues de seleccionar una actividad
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Maneja el boton de regreso
     * Si el drawer esta abierto lo cierra, de lo contrario
     * Si el stack de fragmentos esta vacio regresa a el stack anterior de lo contrario cierra la app
     */
    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if(drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START);
        else
            if(count == 0) super.onBackPressed();
            else getSupportFragmentManager().popBackStackImmediate();
    }

    @Override
    public void linkActivity(Activity activity) {
        //Aqui realizamos el envio de una actividad a los detalles de dicha actividad (Objetos)
        detailedFragment = new DetailedFragment();
        Bundle args = new Bundle();
        args.putSerializable("objeto", activity);
        detailedFragment.setArguments(args);
        //Cambiamos de fragment
        getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.fragment_container, detailedFragment).commit();
    }

    /**
     * Metodo para limpiar el stack de fragmentos
     * usado para cuando se llega al menu inicial
     */
    public void cleanFragmentStack(){
        for(int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); ++i){
            getSupportFragmentManager().popBackStack();
        }
    }
}